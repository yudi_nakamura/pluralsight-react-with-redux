import React from "react"
import { Link } from "react-router-dom"

const PageNotFound = () => (
  <>
    <h1>Oops! Page not found.</h1>
    <Link to="/" className="btn btn-primary btn-lg mt-3">Back to Home</Link>
  </>
)

export default PageNotFound
