import React from "react"

const AboutPage = () => {
  return (
    <div className="jumbotron">
      <h2>About</h2>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea quasi at accusantium voluptatem aliquid reprehenderit cumque alias, tempore itaque ipsa asperiores corporis doloribus magnam dolor velit nisi doloremque. Recusandae, molestiae!</p>
    </div>
  )
}

export default AboutPage
