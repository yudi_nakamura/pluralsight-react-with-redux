import React from "react"
import { shallow } from "enzyme"
import CourseForm from "./CourseForm"

function renderCourseForm(args) {
  const defaultProps = {
    authors: [],
    course: {},
    saving: false,
    erros: {},
    onSave: () => {},
    onChange: () => {}
  }

  const props = { ...defaultProps, ...args }

  return shallow(<CourseForm {...props} />)
}

it("=== Renders form and header ===", () => {
  const wrapper = renderCourseForm()

  expect(wrapper.find('form').length).toBe(1)
  expect(wrapper.find('h2').text()).toEqual('Add Course')
})

it('=== Labels save button as "Save" when not saving ===', () => {
  const wrapper = renderCourseForm()
  // console.log(`=== wrapper ===`, wrapper.debug())
  expect(wrapper.find('button').text()).toBe('Save')
})

it('=== Labels save button as "Saving..." when saving ===', () => {
  const wrapper = renderCourseForm({ saving: true })
  expect(wrapper.find('button').text()).toBe('Saving...')
})
