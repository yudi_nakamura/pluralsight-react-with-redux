import React from "react"
import { cleanup, render } from "react-testing-library"
import CourseForm from "./CourseForm"

afterEach(cleanup)

function renderCourseForm(args) {
  const defaultProps = {
    authors: [],
    course: {},
    saving: false,
    erros: {},
    onSave: () => { },
    onChange: () => { }
  }

  const props = { ...defaultProps, ...args }

  return render(<CourseForm {...props} />)
}

it('=== Should render Add Course header ===', () => {
  const { getByText } = renderCourseForm()
  getByText('Add Course')
})

it('=== Should label save button as "Save" when not saving ===', () => {
  const { getByText } = renderCourseForm()
  getByText('Save')
})

it('=== Should label save button as "Saving..." when saving ===', () => {
  //eslint-disable-next-line no-unused-vars
  const { getByText, debug } = renderCourseForm({ saving: true })
  // debug()
  getByText('Saving...')
})
