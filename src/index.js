import React from "react"
import { render } from "react-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Provider as ReduxProvider } from "react-redux";
// * * * * * Styles * * * * *
import "bootstrap/dist/css/bootstrap.min.css"
import "./index.css"
// * * * * * Componets * * * * *
import App from "./components/App"
// * * * * * Store * * * * *
import configureStore from "./redux/configureStore"

const store = configureStore()

render(
  <ReduxProvider store={store}>
    <Router>
      <App></App>
    </Router>
  </ReduxProvider>,
  document.getElementById('app')
)
